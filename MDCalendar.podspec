Pod::Spec.new do |s|

  s.name         = "MDCalendar"
  s.version      = "0.0.7"
  s.summary      = "Man Dian Calendar."

  s.description  = <<-DESC
                   Foods Database include US and chinese foods 
                   DESC

  s.platform     = :ios, "7.0"
  s.source       = { :git => "https://Cheng_Hu@bitbucket.org/Cheng_Hu/mdcalendar.git", :tag => '0.0.7' }
  s.source_files  = "MDCalendar/**/*.{h,m}"
  s.resources = 'MDCalendar.bundle'
  s.frameworks = "SystemConfiguration"
  s.requires_arc = true
end