//
//  KalTheme.m
//  MDCalendar
//
//  Created by 程虎 on 14-8-6.
//  Copyright (c) 2014年 user. All rights reserved.
//

#import "KalTheme.h"

@implementation KalTheme

+(KalTheme*)sharedInstance
{
	static dispatch_once_t pred;
	static KalTheme *sharedInstance = nil;
    
	dispatch_once(&pred, ^{ sharedInstance = [[self alloc] init]; });
	return sharedInstance;
}


@end
