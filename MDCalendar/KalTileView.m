/*
 * Copyright (c) 2009 Keith Lazuka
 * License: http://www.opensource.org/licenses/mit-license.html
 */

#import "KalTileView.h"
#import "KalPrivate.h"
#import <CoreText/CoreText.h>
#import "Kal.h"

extern const CGSize kTileSize;

@implementation KalTileView

- (id)initWithFrame:(CGRect)frame
{
    if ((self = [super initWithFrame:frame])) {
        self.opaque = NO;
        self.backgroundColor = [UIColor clearColor];
        self.clipsToBounds = NO;
        origin = frame.origin;
        [self setIsAccessibilityElement:YES];
        [self setAccessibilityTraits:UIAccessibilityTraitButton];
        [self resetState];
    }
    return self;
}

- (void)drawRect:(CGRect)rect
{
    if (self.underline) {
        CGContextRef ctx = UIGraphicsGetCurrentContext();
        CGContextSetLineWidth(ctx, 0.5);
        CGContextMoveToPoint(ctx, 0, kTileSize.height);
        CGContextAddLineToPoint(ctx, kTileSize.width, kTileSize.height);
        CGContextStrokePath(ctx);
    }
    
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    CGFloat fontSize = 15;
    UIFont *font = [UIFont fontWithName:@"HelveticaNeue" size:fontSize];
    UIColor *textColor = nil;
    CGContextSelectFont(ctx, [font.fontName cStringUsingEncoding:NSUTF8StringEncoding], fontSize, kCGEncodingMacRoman);
    
    if (self.belongsToAdjacentMonth) {
        textColor = [UIColor clearColor];
    } else if (self.isDisable) {
        textColor = kGrayColor;
    } else {
        textColor = kDarkGrayColor;
    }
    
    if (self.state == KalTileStateHighlighted || self.state == KalTileStateSelected) {
        
        CGRect frame = CGRectMake(0, 0, 30, 30);
        frame.origin.x = (kTileSize.width - frame.size.width) / 2;
        frame.origin.y = (kTileSize.height - frame.size.height) / 2;

        CGContextRef ctx = UIGraphicsGetCurrentContext();
        CGContextAddEllipseInRect(ctx, frame);
        CGContextSetFillColor(ctx, CGColorGetComponents([self.themeColor CGColor]));
        CGContextFillPath(ctx);

        textColor = [UIColor whiteColor];
    } 
    
    NSUInteger n = [self.date day];
    NSString *dayText = [NSString stringWithFormat:@"%lu", (unsigned long)n];

    CGSize textSize = [dayText sizeWithFont:font];
    CGFloat textX, textY;
    textX = roundf(0.5f * (kTileSize.width - textSize.width));
    textY = roundf(0.5f * (kTileSize.height - textSize.height));
    [textColor setFill];
    [dayText drawAtPoint:CGPointMake(textX, textY) withFont:font];
    
}

- (void)resetState
{
    // realign to the grid
    CGRect frame = self.frame;
    frame.origin = origin;
    frame.size = kTileSize;
    self.frame = frame;
    
    self.date = nil;
    _type = KalTileTypeRegular;
    self.state = KalTileStateNone;
}

- (void)setDate:(NSDate *)aDate
{
    if (_date == aDate)
        return;
    
    _date = aDate;
    
    [self setNeedsDisplay];
}

- (void)setState:(KalTileState)state
{
    if (_state != state) {
        _state = state;
        [self setNeedsDisplay];
    }
}

- (void)setType:(KalTileType)tileType
{
    if (_type != tileType) {
        _type = tileType;
        [self setNeedsDisplay];
    }
}

- (BOOL)isToday { return self.type & KalTileTypeToday; }
- (BOOL)isFirst { return self.type & KalTileTypeFirst; }
- (BOOL)isLast { return self.type & KalTileTypeLast; }
- (BOOL)isDisable { return self.type & KalTileTypeDisable; }
- (BOOL)isMarked { return self.type & KalTileTypeMarked; }

- (BOOL)belongsToAdjacentMonth { return self.type & KalTileTypeAdjacent; }

@end
