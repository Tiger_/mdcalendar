//
//  KalTheme.h
//  MDCalendar
//
//  Created by 程虎 on 14-8-6.
//  Copyright (c) 2014年 user. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KalTheme : NSObject

+(KalTheme*)sharedInstance;

@end
