//
//  MDCalendar.h
//  MDCalendar
//
//  Created by 程虎 on 14-8-7.
//  Copyright (c) 2014年 pacer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KalView.h"       // for the KalViewDelegate protocol
#import "KalLogic.h"
#import "NSDate+Convenience.h"
#import "NSDateAdditions.h"

@protocol MDCalendarDelegate;

@interface MDCalendar : UIView <KalViewDelegate>

@property (nonatomic, weak) id<MDCalendarDelegate> delegate;
@property (nonatomic, strong) NSDate *selectedDate;

- (id)initWithTheme:(UIColor *)color;

@end

@protocol MDCalendarDelegate

@optional

- (void)didSelectDate:(NSDate *)date;

@end