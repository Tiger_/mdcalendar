//
//  MDCalendar.m
//  MDCalendar
//
//  Created by 程虎 on 14-8-7.
//  Copyright (c) 2014年 pacer. All rights reserved.
//

#import "MDCalendar.h"

@interface MDCalendar()

@property (nonatomic, strong) KalLogic *logic;
@property (nonatomic, strong) KalView *kalView;
@property (nonatomic, strong) UIColor *themeColor;

@end

@implementation MDCalendar

- (id)initWithTheme:(UIColor *)color {
    self.themeColor = color;
    return [self initWithFrame:[[UIScreen mainScreen] bounds]];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupView];
    }
    return self;
}

- (void) setupView {
    self.logic = [[KalLogic alloc] initForDate:[NSDate date]];
    
    self.kalView = [[KalView alloc] initWithFrame:[[UIScreen mainScreen] bounds] delegate:self logic:self.logic theme:self.themeColor];
    self.kalView.gridView.selectionMode = KalSelectionModeSingle;
    self.kalView.delegate = self;
    
    self.kalView.gridView.maxAVailableDate = [NSDate date];
    
    [self addSubview:self.kalView];
}

- (void)setSelectedDate:(NSDate *)selectedDate {
    _selectedDate = [NSDate dateStartOfDay:selectedDate];
    self.kalView.gridView.beginDate = _selectedDate;
    [self showAndSelectDate:_selectedDate];
}

#pragma mark -

- (void)showAndSelectDate:(NSDate *)date
{
    if ([self.kalView isSliding])
        return;
    
    [self.logic moveToMonthForDate:date];
    
#if PROFILER
    uint64_t start, end;
    struct timespec tp;
    start = mach_absolute_time();
#endif
    
    [self.kalView jumpToSelectedMonth];
    
#if PROFILER
    end = mach_absolute_time();
    mach_absolute_difference(end, start, &tp);
    printf("[[self calendarView] jumpToSelectedMonth]: %.1f ms\n", tp.tv_nsec / 1e6);
#endif
    
}

- (void)didSelectDate:(NSDate *)date
{
    _selectedDate = date;
    [self.delegate didSelectDate:date];
    [self setNeedsDisplay];
}

- (void)showPreviousMonth
{
    [self.logic retreatToPreviousMonth];
    [self.kalView slideDown];
}

- (void)showFollowingMonth
{
    [self.logic advanceToFollowingMonth];
    [self.kalView slideUp];
}

- (void)significantTimeChangeOccurred
{
    [self.kalView jumpToSelectedMonth];
}
@end
